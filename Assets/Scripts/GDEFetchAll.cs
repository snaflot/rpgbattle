using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class GDEFetchAll : MonoBehaviour {

  // GDEで利用する全キャラデータの保存先を作成
  // スキーマ追加時に必要な記述1
  public static List<GDEChrParamData> chrParams = new List<GDEChrParamData>();
  public static List<GDEPlayerStatData> playerStat = new List<GDEPlayerStatData>();
  public static List<GDEEnemyParamData> enemyParams = new List<GDEEnemyParamData>();
  public static List<GDEEnemyStatData> enemyStat = new List<GDEEnemyStatData>();
  public static List<GDESaveInfoData> saveInfo = new List<GDESaveInfoData>();
  public static List<GDESpecialData> special = new List<GDESpecialData>();
  //public static List<GDECharacterGrowthData> ChrGrowth = new List<GDECharacterGrowthData>();
/*
  public static List<GDEStageData> stages = new List<GDEStageData>();
  public static List<GDEItemData> items = new List<GDEItemData>();
  public static List<GDEShelfData> shelf = new List<GDEShelfData>();
  public static List<GDEBasketData> basket = new List<GDEBasketData>();
  public static List<GDEEnemyParamData> enemyParams = new List<GDEEnemyParamData>();
*/

	void Awake()
	{
    //if(GDEFetchAll.chrParams.Count > 0) return;
		// 初期化
		GDEDataManager.Init("gde_data");
    Debug.Log("GDEの初期化を行います");
		GetGDEData();
	}

	public static void GetGDEData()
	{
		chrParams  = GDEDataManager.GetAllItems<GDEChrParamData>();
		playerStat = GDEDataManager.GetAllItems<GDEPlayerStatData>();
		enemyParams  = GDEDataManager.GetAllItems<GDEEnemyParamData>();
		enemyStat = GDEDataManager.GetAllItems<GDEEnemyStatData>();
		saveInfo = GDEDataManager.GetAllItems<GDESaveInfoData>();
		special = GDEDataManager.GetAllItems<GDESpecialData>();

	}

}
