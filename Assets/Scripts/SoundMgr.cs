using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundMgr : MonoBehaviour {

	public List<string> bgmList = new List<string>();
	public List<string> seList = new List<string>();
	public List<string> seAttackList = new List<string>();
	public List<string> voiceList = new List<string>();

	//開始
	void Awake () {
		LoadBgmAndSe();

		if(bgmList.Count == 0){
			print("SoundMgr.csでBGMの設定をしましょう");
		}

		Sound.LoadSe("heal", "MAGIC_SPELL_Morphing_Synth_Harp_Scales_Subtle_stereo");

		// Sound.PlayBgm("bgm01")で再生する
		// Sound.PlaySe("voice01")で再生する
	}

	void LoadBgmAndSe(){
		for(var i=0; i<bgmList.Count; i++){
			string name = "bgm" + i;
			Sound.LoadBgm(name, bgmList[i]);
		}

		for(var i=0; i<seList.Count; i++){
			string name = "se" + i;
			Sound.LoadSe(name, seList[i]);
		}

		for(var i=0; i<seAttackList.Count; i++){
			string name = "seAttack" + i;
			Sound.LoadSe(name, seAttackList[i]);
		}
	}

	public void ChangeBgm(){
		int id = Random.Range(0, bgmList.Count);
		Debug.Log(bgmList[id]);
		string bgm = "bgm" + id;
		Sound.PlayBgm(bgm);
	}
}
