﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleMgr : BasicMgr {
	[SerializeField] List<GameObject> playerList = new List<GameObject>(); //Todo:GDEから動的に生成するように変更予定
	[SerializeField] List<Unit> playerScriptList = new List<Unit>();
	[SerializeField] List<GameObject> enemyList = new List<GameObject>();
	[SerializeField] List<Unit> enemyScriptList = new List<Unit>();
	List<int> playerPartyList = new List<int>();
	[SerializeField] GameObject slash;
	[SerializeField] GameObject healParticle;
	[SerializeField] GameObject particleGroup;
	public GameObject player;
	public GameObject enemyPrefab;
	int playerNum;
	int enemyNum;
	SoundMgr soundMgr;

	GameObject actor;
	GameObject target;
	Unit targetScript;
	Unit actorScript;
	int actorId;
	int targetId;
	int initWt = 1000;

	void Start () {
		InitHenseiPlayer();
		// ランダムにBGMをならす
		soundMgr = GameObject.Find("SoundMgr").gameObject.GetComponent<SoundMgr>();
		soundMgr.ChangeBgm();

		StartCoroutine(BattleLoop());

	}

	// 勝敗がつくまでループ
	IEnumerator BattleLoop(){
		yield return new WaitForSeconds(1f);
		while(true){
			SelectActUnit(); //スピードに応じて、行動ユニットを決める
			Action();
			isDestroyEnemy(); // 敵の全滅判定
			ResetTarget();
			yield return new WaitForSeconds(2f);
		}
	}

	void ResetTarget(){
		if(actor.tag == "Player"){
			UpdateWt(actorId, "Player");
		} else {
			UpdateWt(actorId, "Enemy");
		}
		actorId = 999;
		actor = null;
		targetId = 999;
		target = null;
		targetScript = null;
	}

	// 行動するユニットとIDを決定する
	void SelectActUnit(){
		// 最もWTが少ないユニットと同じ値、WTを全ユニットから減らす
		int minWt = 10000;
		int wt;
		int agi;
		// 全プレイヤーの最小WTを算出
		for(var i=0; i<playerNum; i++){
			if(IsDead(i, "Player")) continue; // ユニットが死んでいる場合は、WTを減らさない
			wt = playerScriptList[i].wt;
			// 確認中ユニットのWTが最も小さければ、最小WTを更新
			if(wt < minWt){
				minWt = wt;
				actorId = i;
				actor = playerList[i];
				actorScript = actor.GetComponent<Unit>();
			}
		}
		// 全敵ユニットの最小WTを算出
		for(var i=0; i<enemyNum; i++){
			if(IsDead(i, "Enemy")) continue; // ユニットが死んでいる場合は、WTを減らさない
			wt = enemyScriptList[i].wt;
			if(wt < minWt){
				minWt = wt;
				actorId = i;
				actor = enemyList[i];
				actorScript = actor.GetComponent<Unit>();
			}
		}
		// 全プレイヤーのWTをマイナス
		for(var i=0; i<playerNum; i++){
			if(IsDead(i, "Player")) continue; // ユニットが死んでいる場合は、WTを減らさない
			playerScriptList[i].wt -= minWt;
			Debug.Log("wt:" + playerScriptList[i].wt);
		}
		// 全敵のWTをマイナス
		for(var i=0; i<enemyNum; i++){
			enemyScriptList[i].wt -= minWt;
			Debug.Log("wt:" + enemyScriptList[i].wt);
		}

	}

  // 選択したボタンに応じて、アクションが決定する
	void Action(){
		// 攻撃
		if(actorScript.status == Unit.State.ATTACK){
			Attack();
		}
		// スキルを使用
		if(actorScript.status == Unit.State.SKILL){
			actorScript.Special();
		}
	}

	// 攻撃する
	void Attack(){
		target = SelectTarget();
		targetScript = target.GetComponent<Unit>();
		if(target == null) return;
		Debug.Log("行動：" + actor.name + "/ターゲット：" + target.name + target.tag);
		int atk = targetScript.atk;
		targetScript.bar.valueCurrent -= atk;
		Sound.PlaySe("seAttack2");
		CreateParticle();
		// HPが0以下になったら、死亡処理
		if(targetScript.bar.valueCurrent <= 0){
			DeadUnit(targetId, target.tag);
		}
	}

	// ターゲットを選択する
	GameObject SelectTarget(string targetType=null){
		if(targetType == null){
			targetType = this.actor.tag;
		}
		// 行動ユニットがプレイヤーの場合
		if(targetType == "Player"){
			// ユニットが死亡している場合は,idを選択し直す
			do{
				targetId = Random.Range(0, enemyList.Count);
				target = enemyList[targetId];
				targetScript = enemyScriptList[targetId];
				Debug.Log("ターゲットを選定します");
			}
			while(targetScript.isDead == true);
			Debug.Log("ターゲットID: " + targetId);
			return enemyList[targetId];
		// 行動ユニットが敵の場合、プレイヤリストからランダムにターゲットを選択する
	} else if(targetType == "Enemy"){
			// ユニットが死亡している場合は処理を継続
			do{
				targetId = Random.Range(0, playerList.Count);
				target = playerList[targetId];
				targetScript = playerScriptList[targetId];
				//Debug.Log("ターゲットを選定します");
			}
			while(targetScript.isDead == true);
			return playerList[targetId];
		}
		return null;
	}


	// 死亡処理
	void DeadUnit(int id, string tag){
		if(tag == "Player"){
			playerScriptList[id].isDead = true;
		} else if(tag == "Enemy"){
			enemyScriptList[id].isDead = true;
		} else {
			Debug.Log("死亡対象オブジェクトにタグが設定されていないのでは？");
		}
		target.SetActive(false);
		//target = null;
		//targetScript = null;
	}

	// 死亡済みのユニットかチェック
	bool IsDead(int id, string unitType){
		if(unitType == "Player"){
			if(playerScriptList[id].isDead == true){
				Debug.Log(id + "対象ユニットは死亡しています");
				return true;
			} else {
				return false;
			}
		} else if(unitType == "Enemy"){
			if(enemyScriptList[id].isDead == true){
				Debug.Log(id + "対象ユニットは死亡しています");
				return true;
			} else {
				return false;
			}
		}
		Debug.Log("メソッド側の引数を間違え？タグは設定してる？");
		return false;
	}

	void GetHpBar(){
		string name = target.name + "HpBar";
		EnergyBar targetEnergyBar = GameObject.Find(target.name + "HpBar").GetComponent<EnergyBar>();
	}

	// 勝敗のチェック
	void isDestroyEnemy(){
		for(var i=0; i<enemyScriptList.Count; i++){
			if(enemyScriptList[i].isDead == false) return;
		}
		ChangeScene("Main");
	}

	void CreateParticle(){
		Vector3 P = target.transform.position;
		FastPool fastPool = FastPoolManager.GetPool(slash);
		GameObject obj = fastPool.FastInstantiate(P, Quaternion.identity);
		//GameObject obj = fastPool.FastInstantiate(P, Quaternion.identity, particleGroup);
	}

	// 攻撃後、WTをリセットする場合のみに使用
	void UpdateWt(int id, string unitType){
		int agi =0;
		int wt = 0;
		if(unitType=="Player"){
			agi = playerScriptList[id].agi;
			wt = playerScriptList[id].wt = initWt-agi;
		} else if(unitType=="Enemy"){
			agi = enemyScriptList[id].agi;
			wt = enemyScriptList[id].wt = initWt-agi;
		} else {
			Debug.Log("引数が間違ってない？");
		}
		Debug.Log(unitType + id + "のWT:" + wt);
	}

	int ChrParamsSelect(int id, string field){
		//return GDEFetchAll.chrParams[id].field;
		return 0;
	}

	// 編成済みのプレイヤー数、敵数を変数に代入
	void InitHenseiPlayer(){
		SetPlayer();
		SetEnemy();
		playerNum = playerList.Count;
		enemyNum = enemyList.Count;
		Debug.Log("プレイヤー:" + playerNum + "敵:" + enemyNum);
	}

	void InitPlayerStat(){
		for(var i=0; i<3; i++){ //Todo: 仮に3としている
			int partyId = playerPartyList[i];
		}
	}

	void SetPlayer(){
		int id = 0;
		//GameObject obj = Instantiate(playerList[2], transform.position, transform.rotation) as GameObject;
		GameObject obj = Instantiate(this.player, new Vector2(3, 0), Quaternion.identity) as GameObject;
		SpriteRenderer sRenderer = obj.GetComponent<SpriteRenderer>();
		sRenderer.sprite = Resources.Load<Sprite>("Sprites/" + GDEFetchAll.chrParams[id].enName);
		//Player player = obj.GetComponent<Player>();
		Unit unit = obj.GetComponent<Unit>();
		// プレイヤーユニットの能力をGDEから取得して、変数に代入
		unit.partyId = id;
		unit.name = GDEFetchAll.chrParams[id].enName;
		unit.maxHp = GDEFetchAll.chrParams[id].hp;
		unit.atk = GDEFetchAll.chrParams[id].atk;
		unit.agi = GDEFetchAll.chrParams[id].agi;
		unit.wt = initWt-unit.agi;
		unit.specialList.Add(GDEFetchAll.chrParams[id].special[0]);
		playerList.Add(obj);
		playerScriptList.Add(unit);
	}

	void SetEnemy(){
		for(var i=0; i<3; i++){
			int id = 0;
			float y = -6 + (i*3);
			GameObject obj = Instantiate(this.enemyPrefab, new Vector2(-2,y), Quaternion.identity) as GameObject;
			SpriteRenderer sRenderer = obj.GetComponent<SpriteRenderer>();
			sRenderer.sprite = Resources.Load<Sprite>("Sprites/" + GDEFetchAll.enemyParams[id].enName);
			if(obj==null){
				Debug.Log("オブジェクトが空です");
			}
			Debug.Log(obj.name);
			Unit unit = obj.GetComponent<Unit>();
			// 敵の能力をGDEから取得して、変数に代入
			unit.partyId = i;
			unit.name = GDEFetchAll.enemyParams[id].enName + unit.partyId;
			unit.maxHp = GDEFetchAll.enemyParams[id].hp;
			unit.atk = GDEFetchAll.enemyParams[id].atk;
			unit.agi = GDEFetchAll.enemyParams[id].agi;
			unit.wt = initWt-unit.agi;
			enemyList.Add(obj);
			enemyScriptList.Add(unit);
		}
	}

}
