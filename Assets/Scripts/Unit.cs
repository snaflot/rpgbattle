﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour {

	public int partyId;
	public int wt;
	public int atk;
	public int agi;
	public bool isDead;
	public int maxHp;
	public EnergyBar bar;
	//public List<GameDataEditor.GDESpecialData> specialList = new List<GameDataEditor.GDESpecialData>();
	public List<string> specialList = new List<string>();
	public State status;
	[SerializeField] GameObject healParticle;
	SpecialMgr specialMgr;

	public enum State {
		ATTACK,
		SKILL,
	}

	public State GetStatus(){
		return this.status;
	}

	void Start () {
		specialMgr = GameObject.Find("SpecialMgr").GetComponent<SpecialMgr>();
		GameObject hpBar = gameObject.transform.FindChild("EnergyBarCanvas/HpBar").gameObject;
		//energyBar = GameObject.FindWithTag("Hp");
		bar = hpBar.GetComponent<EnergyBar>();
		bar.valueCurrent = maxHp;
		bar.valueMax = maxHp;
		status = State.ATTACK;
	}

	public void ActionSkill(){
		this.status = State.SKILL;
	}

	public void ActionAttack(){
		this.status = State.ATTACK;
	}

	public void Special(){
		switch(specialList[0]){
			case "heal":
				Vector2 pos = transform.position;
				pos += new Vector2(0,-1.0f);
				Instantiate(healParticle, pos, Quaternion.identity);
				Sound.PlaySe("heal");
				this.bar.valueCurrent += 100;
				break;
			default:
				Debug.Log("特技が定義されていません!");
				break;
		}
	}


}
