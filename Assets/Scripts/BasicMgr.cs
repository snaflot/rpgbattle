﻿using UnityEngine;
using System.Collections;

public class BasicMgr : MonoBehaviour {

	public void ChangeScene(string sceneName){
		FadeManager.Instance.LoadLevel(sceneName, 0.1f);
	}

}
