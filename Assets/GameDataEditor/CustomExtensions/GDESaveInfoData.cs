// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      /Users/snaflot/Unity/Battle/Assets/GameDataEditor/Resources/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDESaveInfoData : IGDEData
    {
        static string goldKey = "gold";
		int _gold;
        public int gold
        {
            get { return _gold; }
            set {
                if (_gold != value)
                {
                    _gold = value;
					GDEDataManager.SetInt(_key, goldKey, _gold);
                }
            }
        }

        static string stageKey = "stage";
		int _stage;
        public int stage
        {
            get { return _stage; }
            set {
                if (_stage != value)
                {
                    _stage = value;
					GDEDataManager.SetInt(_key, stageKey, _stage);
                }
            }
        }

        static string partyKey = "party";
		public List<int>      party;
		public void Set_party()
        {
	        GDEDataManager.SetIntList(_key, partyKey, party);
		}
		

        public GDESaveInfoData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "SaveInfo");
			
            dict.Merge(true, gold.ToGDEDict(goldKey));
            dict.Merge(true, stage.ToGDEDict(stageKey));

            dict.Merge(true, party.ToGDEDict(partyKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetInt(goldKey, out _gold);
                dict.TryGetInt(stageKey, out _stage);

                dict.TryGetIntList(partyKey, out party);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _gold = GDEDataManager.GetInt(_key, goldKey, _gold);
            _stage = GDEDataManager.GetInt(_key, stageKey, _stage);

            party = GDEDataManager.GetIntList(_key, partyKey, party);
        }

        public GDESaveInfoData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDESaveInfoData newClone = new GDESaveInfoData(newKey);

            newClone.gold = gold;
            newClone.stage = stage;

            newClone.party = new List<int>(party);
			newClone.Set_party();

            return newClone;
		}

        public GDESaveInfoData DeepClone()
		{
			GDESaveInfoData newClone = ShallowClone();
            return newClone;
		}

        public void Reset_gold()
        {
            GDEDataManager.ResetToDefault(_key, goldKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(goldKey, out _gold);
        }

        public void Reset_stage()
        {
            GDEDataManager.ResetToDefault(_key, stageKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(stageKey, out _stage);
        }

        public void Reset_party()
        {
	        GDEDataManager.ResetToDefault(_key, partyKey);

	        Dictionary<string, object> dict;
	        GDEDataManager.Get(_key, out dict);
	        dict.TryGetIntList(partyKey, out party);
        }
		

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, goldKey);
            GDEDataManager.ResetToDefault(_key, stageKey);
            GDEDataManager.ResetToDefault(_key, partyKey);


            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
